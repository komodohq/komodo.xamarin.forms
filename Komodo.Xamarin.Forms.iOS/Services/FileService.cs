﻿using System;
using System.Diagnostics;
using System.IO;
using Foundation;
using Komodo.Xamarin.Forms.Services;
using System.Linq;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.iOS.Services;

[assembly: Dependency(typeof(FileService))]

namespace Komodo.Xamarin.Forms.iOS.Services
{
    public class FileService : IFileService
    {
        public string ReadAllText(string filename)
        {
			try
			{
				string ext = Path.GetExtension(filename).ToLower().Replace(".", "");
				string file = Path.GetFileNameWithoutExtension(filename);
				string dir = Path.GetDirectoryName(filename);

				string path;

				if (string.IsNullOrWhiteSpace(dir))
				{
					path = NSBundle.MainBundle.PathForResource(file, ext, dir);
				}
				else
				{
					string[] paths = NSBundle.MainBundle.PathsForResources(ext, dir);

					path = paths.FirstOrDefault(p =>
					{
						string fn = Path.GetFileNameWithoutExtension(p);
						return fn == file;
					});
				}

				if (string.IsNullOrWhiteSpace(path))
				{
					throw new IOException($"{filename} not found.");
				}

				return File.ReadAllText(path);
			}
			catch(IOException ex)
			{
				return string.Empty;
			}
        }

		public byte[] ReadAllBytes(string filename)
		{
			try

			{
				string ext = Path.GetExtension(filename).ToLower().Replace(".", "");
				string file = Path.GetFileNameWithoutExtension(filename);
				string dir = Path.GetDirectoryName(filename);

				string path;

				if (string.IsNullOrWhiteSpace(dir))
				{
					path = NSBundle.MainBundle.PathForResource(file, ext, dir);
				}
				else
				{
					string[] paths = NSBundle.MainBundle.PathsForResources(ext, dir);

					path = paths.FirstOrDefault(p =>
					{
						string fn = Path.GetFileNameWithoutExtension(p);
						return fn == file;
					});
				}

				if (string.IsNullOrWhiteSpace(path))
				{
					throw new IOException($"{filename} not found.");
				}

                
				return File.ReadAllBytes(path);
			}
			catch (IOException ex)
			{
				Debug.WriteLine(ex);

				return new byte[0];
			}
			
		}
    }
}
