﻿using System;
using Komodo.Xamarin.Forms.Services;
using UIKit;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.iOS.Services;
using Komodo.Xamarin.Forms.ViewModels.Controls;

[assembly: Dependency(typeof(DialogService))]

namespace Komodo.Xamarin.Forms.iOS.Services
{
	public class DialogService : IDialogService
    {
        public DialogService()
        {
        }

		public void Show(DialogViewModel dialog)
		{
			UIApplication.SharedApplication.InvokeOnMainThread(() =>
            {
				UIWindow window = UIApplication.SharedApplication.KeyWindow;
				UIViewController vc = window.RootViewController;
                while (vc.PresentedViewController != null)
                {
                    vc = vc.PresentedViewController;
                }

				UIAlertController alert = UIAlertController.Create(dialog.Title.Label, dialog.Message.Label, UIAlertControllerStyle.Alert);

                int current = 1;

                // Configure the alert
				foreach (DialogButtonViewModel button in dialog.Buttons)
                {
                    switch (current)
                    {
                        case 1:
                            alert.AddAction(UIAlertAction.Create(button.Label, UIAlertActionStyle.Default, (action) => { button.Action(); }));
                            break;
                        case 2:
                            alert.AddAction(UIAlertAction.Create(button.Label, UIAlertActionStyle.Default, (action) => { button.Action(); }));
                            break;
                        case 3:
                            alert.AddAction(UIAlertAction.Create(button.Label, UIAlertActionStyle.Default, (action) => { button.Action(); }));
                            break;
                    }

                    current++;
                }

				dialog.OnClose += (sender, e) => alert.DismissViewController(true, null);

                // Display the alert
                vc.PresentViewController(alert, true, null);
            });
		}
	}
}
