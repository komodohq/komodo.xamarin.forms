﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Foundation;
using Komodo.Xamarin.Forms;
using Komodo.Xamarin.Forms.Services;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.iOS.Services;

[assembly: Dependency(typeof(LocaleService))]

namespace Komodo.Xamarin.Forms.iOS.Services
{
    public class LocaleService : ILocaleService
    {
        private JObject doc;

		private string defaultLanguageCode = "en";

		private List<string> languageSupport = new List<string>()
		{
            "en"
		};

        public LocaleService()
        {
            IFileService service = DependencyService.Get<IFileService>();
            string data = service.ReadAllText($"locale/{LanguageCode.ToLower()}.json");
            doc = JObject.Parse(data);
        }

        public string CountryCode
        {
            get
            {
                return NSLocale.CurrentLocale.CountryCode;
            }
        }

        public string LanguageCode
        {
            get
            {
				if (!languageSupport.Contains(NSLocale.CurrentLocale.LanguageCode))
				{
					return defaultLanguageCode;
				}
                return NSLocale.CurrentLocale.LanguageCode;
            }
        }

        public string TimeZone
        {
            get
            {
                return NSTimeZone.LocalTimeZone.Name;
            }
        }

        public string GetString(params string[] key)
        {
            if (key == null || key.Length == 0)
            {
                return string.Empty;
            }

            try
            {
                string value = (string)doc.SelectToken(string.Join(".", key));
                return value;
            }
            catch (Exception ex)
            {
                string path = string.Join("->", key);

				if (!string.IsNullOrWhiteSpace(path))
				{
					Debug.WriteLine($"Error reading locale key at path '{path}'");
				}
            }

            return string.Empty;
        }
    }
}
