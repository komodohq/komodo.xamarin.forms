﻿using System;
using Foundation;
using Komodo.Xamarin.Forms.Services;
using UIKit;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.iOS.Services;

[assembly: Dependency(typeof(DeviceService))]

namespace Komodo.Xamarin.Forms.iOS.Services
{
    public class DeviceService : IDeviceService
    {
        public void OpenMailTo(string email, string subject = "", string body = "")
        {
            using (var encoded = new NSString($"mailto:{email}?subject={subject}&body={body}").CreateStringByAddingPercentEscapes(NSStringEncoding.UTF8))
            using (var url = NSUrl.FromString(encoded))
            {
                UIApplication.SharedApplication.OpenUrl(url);
            }
        }
    }
}
