﻿using System;
using Foundation;
using UIKit;
using Xamarin.Forms;
using System.Linq;
using CoreGraphics;
using System.Diagnostics;
using Xamarin.Forms.Platform.iOS;
using ObjCRuntime;
using Komodo.Xamarin.Forms.Controls;
using Komodo.Xamarin.Forms.Services;
using Komodo.Xamarin.Forms.iOS.Services;

[assembly: Dependency(typeof(PrintService))]

namespace Komodo.Xamarin.Forms.iOS.Services
{
	public class PrintService : IPrintService
	{
		public void Show(string path)
		{
			var printInfo = UIPrintInfo.PrintInfo;
			var printer = UIPrintInteractionController.SharedPrintController;
			printInfo.Duplex = UIPrintInfoDuplex.LongEdge;
			printInfo.OutputType = UIPrintInfoOutputType.General;
			printer.PrintInfo = printInfo;
			printer.PrintingItem = new NSUrl(path, false);
			printer.ShowsPageRange = false;
			printer.Present(true, (controller, completed, error) =>
			{
				Debug.WriteLine(completed ? "Printing completed" : $"Printing did not complete : {controller} {error}");
			});
		}

		public void Show(PrintableWebView webView)
		{
			UIWebView control = Runtime.GetNSObject(webView.NativeHandle) as UIWebView;

			UIPrintInteractionController printer = UIPrintInteractionController.SharedPrintController;

			string name = control.EvaluateJavascript("document.title");

            printer.ShowsPageRange = true;         
            printer.PrintInfo = UIPrintInfo.PrintInfo;
            printer.PrintInfo.OutputType = UIPrintInfoOutputType.General;         
			printer.PrintInfo.JobName = name;

            printer.PrintPageRenderer = new UIPrintPageRenderer()
            {
                HeaderHeight = 40,
                FooterHeight = 40
            };
            printer.PrintPageRenderer.AddPrintFormatter(control.ViewPrintFormatter, 0);

            if (Device.Idiom == TargetIdiom.Phone)
            {
                printer.PresentAsync(true);
            }
            else if (Device.Idiom == TargetIdiom.Tablet)
            {
				printer.PresentFromRectInViewAsync(new CGRect(200, 200, 0, 0), control, true);
            }
		}
	}   
}
