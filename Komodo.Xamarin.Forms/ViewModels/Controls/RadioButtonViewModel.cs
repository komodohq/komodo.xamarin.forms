﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Komodo.Xamarin.Forms.Commands;

namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
    public class RadioButtonViewModel : LabelViewModel
    {
        public RadioGroup Group { get; private set; }
        private bool isChecked;
        private Action checkedChanged;

        public ICommand Command
        {
            get;
        }

        public virtual bool IsChecked
        {
            get => isChecked;
            set => SetProperty(ref isChecked, value);
        }


        public RadioButtonViewModel(RadioGroup group, bool isChecked = false, params string[] localeKey) : base(localeKey)
        {
            Command = new TapCommand((obj) =>
            {
				if (!IsEnabled)
					return;
				
                Debug.WriteLine("Radio Pressed");

                bool changed = !IsChecked;

                IsChecked = true;
                List<RadioButtonViewModel> others = group.RadioButtons.Where(rb => rb != this).ToList();

                foreach (RadioButtonViewModel other in others)
                {
                    other.IsChecked = false;
                }

                if (changed)
                {
                    checkedChanged?.Invoke();
                }

            });

            this.isChecked = isChecked;
            group.Add(this);
        }

        public class RadioGroup
        {
            private readonly object sync = new object();
            private List<RadioButtonViewModel> radioButtons = new List<RadioButtonViewModel>();

            public IReadOnlyList<RadioButtonViewModel> RadioButtons { get { return radioButtons.ToList(); } }

            private readonly Action groupChanged;

			public int SelectedIndex
			{
				get
				{
					if (radioButtons.Count > 0)
					{
						return radioButtons.FindIndex(rb => rb.IsChecked);
					}
					return -1;
				}
			}

            public RadioGroup(Action groupChanged)
            {
                this.groupChanged = groupChanged;
            }

            public void Add(RadioButtonViewModel radioButtonViewModel)
            {
                lock (sync)
                {
                    if (!RadioButtons.Contains(radioButtonViewModel))
                    {
                        radioButtons.Add(radioButtonViewModel);
                    }
                }

                radioButtonViewModel.checkedChanged = () =>
                {
                    foreach (RadioButtonViewModel vm in radioButtons)
                    {
                        vm.RaiseAllPropertiesChanged();
                    }
                    groupChanged?.Invoke();
                };
            }

            public void Remove(RadioButtonViewModel radioButtonViewModel)
            {
                lock (sync)
                {
                    radioButtons.Remove(radioButtonViewModel);
                }
            }
        }
    }


}
