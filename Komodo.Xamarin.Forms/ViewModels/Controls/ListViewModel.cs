﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
    public class ListViewModel<T> : LabelViewModel
    {
        private ObservableCollection<T> items;
        private bool hasItems;


        public ListViewModel(params string[] localeKey) : base(localeKey)
        {
			items = new ObservableCollection<T>();
            hasItems = items.Count > 0;
        }


		public ObservableCollection<T> Items
		{
			get => items;
			set
			{
				if(items != null)
				{
					items.CollectionChanged -= OnCollectionChanged;
				}

				SetProperty(ref items, value);

				if (items != null)
                {
                    items.CollectionChanged += OnCollectionChanged;
                }
			}
		}

		protected virtual void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			RaisePropertyChanged("HasItems");
		}

		public bool HasItems
        {
            get => hasItems;
            set => SetProperty(ref hasItems, value);
        }
    }
}
