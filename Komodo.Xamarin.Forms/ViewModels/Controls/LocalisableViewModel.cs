﻿using System;
using Komodo.Xamarin.Forms.Services;
using Xamarin.Forms;

namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
    public class LocalisableViewModel : ViewModel
    {
        protected ILocaleService Locale { get; set; }

        public LocalisableViewModel()
        {
            Locale = DependencyService.Get<ILocaleService>();
        }
    }
}
