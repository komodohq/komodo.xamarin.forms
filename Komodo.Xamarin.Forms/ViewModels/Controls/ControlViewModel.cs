﻿using System;
namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
    public class ControlViewModel : LocalisableViewModel
    {
        private bool isEnabled = true;
        private bool isVisible = true;

        public virtual bool IsEnabled
        {
            get => isEnabled;
            set => SetProperty(ref isEnabled, value);
        }

        public virtual bool IsVisible
        {
            get => isVisible;
            set => SetProperty(ref isVisible, value);
        }
    }
}
