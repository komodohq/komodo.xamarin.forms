﻿using System;
using System.Windows.Input;
using Komodo.Xamarin.Forms.Commands;

namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
    public class ButtonViewModel : LabelViewModel
    {
        public ICommand Command { get; protected set; }

		public int Delay {
			get {
				return ((TapCommand)Command).Delay;
			}
			set {
				((TapCommand)Command).Delay = value;
			}
		}
        
        public ButtonViewModel(Action action, params string[] localeKey)
            : base(localeKey)
        {
            Command = new TapCommand(action, 500);
        }

		public ButtonViewModel(Action<object> action, params string[] localeKey)
            : base(localeKey)
        {
            Command = new TapCommand(action, 500);
        }
    }
}
