﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Komodo.Xamarin.Forms.Services;
using Xamarin.Forms;

namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
    public class DialogViewModel : LocalisableViewModel
    {
        private IDialogService service;

        public event EventHandler OnClose;

        public List<DialogButtonViewModel> Buttons { get; set; } = new List<DialogButtonViewModel>();
        public LabelViewModel Title { get; set; }
        public LabelViewModel Message { get; set; }

        public DialogViewModel()
        {
            service = DependencyService.Get<IDialogService>();
        }

        public void AddButton(DialogButtonViewModel dialogButton)
        {
            if (Buttons.Count < 3)
            {
                Buttons.Add(dialogButton);
            }
            else
            {
                Debug.WriteLine("Too many dialog buttons.");
            }
        }

        public void Show()
        {
            service.Show(this);
        }

        public void Dismiss()
        {
            OnClose?.Invoke(this, EventArgs.Empty);
        }
    }
}
