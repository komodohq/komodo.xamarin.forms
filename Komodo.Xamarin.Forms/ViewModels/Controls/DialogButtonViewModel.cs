﻿using System;

namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
    public class DialogButtonViewModel : LabelViewModel
    {
        public Action Action { get; }

        public DialogButtonViewModel(Action action, params string[] localeKey) : base(localeKey)
        {
            Action = action;
        }
    }
}
