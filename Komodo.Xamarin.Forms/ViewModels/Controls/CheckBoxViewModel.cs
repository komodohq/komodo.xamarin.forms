﻿using System;
using System.Windows.Input;
using Komodo.Xamarin.Forms.Commands;

namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
	public class CheckBoxViewModel : LabelViewModel
    {
		private bool isChecked;
		private Action<CheckBoxViewModel> checkedChanged;

		public virtual bool IsChecked
        {
            get => isChecked;
            set => SetProperty(ref isChecked, value);
        }

		public ICommand Command
		{
			get;
		}

        public CheckBoxViewModel(bool isChecked = false, Action<CheckBoxViewModel> checkedChanged = null, params string[] localeKey) : base(localeKey)
        {
			Command = new TapCommand((obj) =>
			{
				IsChecked = !IsChecked;

				checkedChanged?.Invoke(this);
			});

            this.isChecked = isChecked;
            this.checkedChanged = checkedChanged;
        }
    }


}
