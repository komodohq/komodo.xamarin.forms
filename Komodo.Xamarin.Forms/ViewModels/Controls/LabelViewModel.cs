﻿namespace Komodo.Xamarin.Forms.ViewModels.Controls
{
	public class LabelViewModel : ControlViewModel
	{
		private string label;

		public string Label
		{
			get => label;

			set => SetProperty(ref label, value);
		}

		public LabelViewModel(params string[] localeKey)
		{
			Label = Locale.GetString(localeKey);
		}
	}
}
