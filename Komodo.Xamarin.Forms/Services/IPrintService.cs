﻿using Komodo.Xamarin.Forms.Controls;

namespace Komodo.Xamarin.Forms.Services
{
    public interface IPrintService
    {
        void Show(string path);
		void Show(PrintableWebView webView);
    }
}
