﻿using System;
namespace Komodo.Xamarin.Forms.Services
{
    public interface IDeviceService
    {
        void OpenMailTo(string email, string subject = "", string body = "");
    }
}
