﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Komodo.Xamarin.Forms.ViewModels.Controls;
using Xamarin.Forms;

namespace Komodo.Xamarin.Forms.Services
{
    public interface IDialogService
    {
		void Show(DialogViewModel dialog);
    }
}
