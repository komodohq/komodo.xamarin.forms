﻿using System.Xml.Linq;
using Newtonsoft.Json;

namespace Komodo.Xamarin.Forms.Services
{
    public interface ILocaleService
    {
        string CountryCode { get; }
        string LanguageCode { get; }
        string TimeZone { get; }

        string GetString(params string[] key);
    }
}
