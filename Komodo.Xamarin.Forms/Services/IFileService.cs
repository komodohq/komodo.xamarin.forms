﻿using System;
namespace Komodo.Xamarin.Forms.Services
{
    public interface IFileService
    {
        string ReadAllText(string path);
		byte[] ReadAllBytes(string filepath);
    }
}
