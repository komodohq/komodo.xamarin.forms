﻿using System;
using System.Windows.Input;

namespace Komodo.Xamarin.Forms.Commands
{
    public class DelegateCommand : ICommand
    {
        readonly Action action;
        readonly Action<object> action1;

        public DelegateCommand(Action action)
        {
            this.action = action;
        }

        public DelegateCommand(Action<object> action)
        {
            this.action1 = action;
        }

        public event EventHandler CanExecuteChanged;

        public virtual bool CanExecute(object parameter)
        {
            return true;
        }

        public virtual void Execute(object parameter)
        {
            action?.Invoke();
            action1?.Invoke(parameter);
        }
    }
}
