﻿using System;

namespace Komodo.Xamarin.Forms.Commands
{
    public class TapCommand : DelegateCommand
    {
		private readonly object sync = new object();      
        private DateTime lastTap;

		public int Delay { get; set; }

        public TapCommand(Action action, int tapDeactivateTime = 500)
            : base(action)
        {
			this.Delay = tapDeactivateTime;
        }

        public TapCommand(Action<object> action, int tapDeactivateTime = 500)
            : base(action)
        {
			this.Delay = tapDeactivateTime;
        }


        public override void Execute(object parameter)
        {
            lock (sync)
            {
				if (DateTime.UtcNow - lastTap < TimeSpan.FromMilliseconds(Delay))
                {
                    return;
                }

                lastTap = DateTime.UtcNow;

                base.Execute(parameter);
            }
        }
    }
}
