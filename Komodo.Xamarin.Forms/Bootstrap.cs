﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace Komodo.Xamarin.Forms
{
    public abstract class Bootstrap : IBootstrapService
    {
        private Dictionary<Type, Func<Page>> map = new Dictionary<Type, Func<Page>>();

        public Page Resolve(ViewModel model) 
        {
            if(model == null)
            {
                throw new ArgumentNullException($"model must not be null.");
            }
            Type type = model.GetType();

            if (map.ContainsKey(type))
            {
                Page view = map[type]();

                if(view == null)
                {
                    throw new InvalidOperationException("View type is not a type of View.");
                }

                view.BindingContext = model;

                return view;
            }

            throw new ArgumentException($"ViewModel of type {type.Name} is not mapped to a View.");
        }

        public void Register<TViewModel, TView>()
        {
            Type vm = typeof(TViewModel);

            Func<Page> constructor = new Func<Page>(() =>
            {
				try
				{
					object v = Activator.CreateInstance(typeof(TView));

					if (v is Page)
					{
						return (Page)v;
					}
				}
                catch (Exception ex)
				{
					Debug.WriteLine("Error resolving view");
					Debug.WriteLine(ex);
				}

                return null;
            });

            map.Add(typeof(TViewModel), constructor);
        }
    }

    public interface IBootstrapService
    {
        Page Resolve(ViewModel model);
        void Register<TViewModel, TView>();
    }
}
