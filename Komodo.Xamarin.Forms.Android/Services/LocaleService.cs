﻿using System;
using System.Diagnostics;
using Android.Content;
using Android.Telephony;
using Java.Util;
using Komodo.Xamarin.Forms;
using Komodo.Xamarin.Forms.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.Android.Services;

[assembly: Dependency(typeof(LocaleService))]

namespace Komodo.Xamarin.Forms.Android.Services
{
    public class LocaleService : ILocaleService
    {
        private Context context;
        private JObject doc;

		private string defaultLanguageCode = "en";

        private List<string> languageSupport = new List<string>()
        {
            "en"
        };

        public LocaleService(Context context)
        {
            this.context = context;
            IFileService service = DependencyService.Get<IFileService>();
            string data = service.ReadAllText($"locale/{LanguageCode.ToLower()}.json");
            doc = JObject.Parse(data);
        }

        public string CountryCode {
            get {
                return Locale.Default.Country;
            }
        }

        public string LanguageCode {
            get {
				if (!languageSupport.Contains(Locale.Default.Language))
                {
                    return defaultLanguageCode;
                }

                return Locale.Default.Language;
            }
        }

        public string TimeZone {
            get {
                Java.Util.TimeZone tz = Java.Util.TimeZone.Default;
                return tz.ID;
            }
        }

        public string GetString(params string[] key)
        {
            if (key == null || key.Length == 0)
            {
                return string.Empty;
            }

            try
            {
                string value = (string)doc.SelectToken(string.Join(".", key));
                return value;
            }
            catch (Exception ex)
            {
                string path = string.Join("->", key);

				if (!string.IsNullOrWhiteSpace(path))
                {
                    Debug.WriteLine($"Error reading locale key at path '{path}'");
                }
            }

            return string.Empty;
        }
    }
}
