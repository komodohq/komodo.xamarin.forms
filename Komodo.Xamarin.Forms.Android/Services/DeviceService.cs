﻿using System;
using Android.Content;
using Komodo.Xamarin.Forms.Services;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.Android.Services;

[assembly: Dependency(typeof(DeviceService))]

namespace Komodo.Xamarin.Forms.Android.Services
{
    public class DeviceService : IDeviceService
    {
        readonly Context context;

        public DeviceService(Context context)
        {
            this.context = context;
        }
        public void OpenMailTo(string email, string subject = "", string body = "")
        {
            Intent emailIntent = new Intent(Intent.ActionSend);

            emailIntent.PutExtra(Intent.ExtraEmail, new string[] { email });

            if (!string.IsNullOrWhiteSpace(subject))
            {
                emailIntent.PutExtra(Intent.ExtraSubject, subject);
            }

            if (!string.IsNullOrWhiteSpace(body))
            {
                emailIntent.PutExtra(Intent.ExtraText, body);
            }

            emailIntent.SetType("message/rfc822");


            context.StartActivity(Intent.CreateChooser(emailIntent, "Send Email Via"));
        }
    }
}
