﻿using System;
using System.Diagnostics;
using System.IO;
using Android.Content;
using Komodo.Xamarin.Forms.Services;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.Android.Services;

[assembly: Dependency(typeof(FileService))]

namespace Komodo.Xamarin.Forms.Android.Services
{
    public class FileService : IFileService
    {
        readonly Context context;

        public FileService(Context context)
        {
            this.context = context;
        }

        public string ReadAllText(string path)
        {
            try
            {
                string content = string.Empty;
                using (StreamReader reader = new StreamReader(context.Assets.Open(path)))
                {
                    content = reader.ReadToEnd();
                }
                return content;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);

                return string.Empty;
            }
        }


		public byte[] ReadAllBytes(string path)
		{
			try
			{            
				using (Stream stream = context.Assets.Open(path))
				{
					long originalPosition = 0;

                    if (stream.CanSeek)
                    {
                        originalPosition = stream.Position;
                        stream.Position = 0;
                    }

                    try
                    {
                        byte[] readBuffer = new byte[4096];

                        int totalBytesRead = 0;
                        int bytesRead;

                        while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                        {
                            totalBytesRead += bytesRead;

                            if (totalBytesRead == readBuffer.Length)
                            {
                                int nextByte = stream.ReadByte();
                                if (nextByte != -1)
                                {
                                    byte[] temp = new byte[readBuffer.Length * 2];
                                    Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                                    Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                                    readBuffer = temp;
                                    totalBytesRead++;
                                }
                            }
                        }

                        byte[] buffer = readBuffer;
                        if (readBuffer.Length != totalBytesRead)
                        {
                            buffer = new byte[totalBytesRead];
                            Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                        }
                        return buffer;
                    }
                    finally
                    {
                        if (stream.CanSeek)
                        {
                            stream.Position = originalPosition;
                        }
                    }
				}
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);

				return new byte[0];
            }
		}
    }
}
