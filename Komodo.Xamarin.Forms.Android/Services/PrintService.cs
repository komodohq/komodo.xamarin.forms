﻿using System;
using System.IO;
using Android.Print;
using Java.IO;
using Komodo.Xamarin.Forms.Services;
using Android.Runtime;
using Komodo.Xamarin.Forms.Controls;
using Android.OS;
using Android.Content;
using WebkitWebView = Android.Webkit.WebView;

using Xamarin.Forms;
using Komodo.Xamarin.Forms.Android.Services;

[assembly: Dependency(typeof(PrintService))]

namespace Komodo.Xamarin.Forms.Android.Services
{
	public class PrintService : IPrintService
	{
		private readonly Context context;

		public PrintService(Context context)
        {
            this.context = context;
        }

		public void Show(string path)
		{
			using (PrintManager printManager = context.GetSystemService(Context.PrintService) as PrintManager)
            {
				printManager?.Print(path, new FilePrintDocumentAdapter(context, Path.GetFileNameWithoutExtension(path), path), null);
            }
		}

		public void Show(PrintableWebView webView)
		{
			if (webView == null)
            {
                System.Diagnostics.Debug.WriteLine("WebView is required");
                return;
            }

            WebkitWebView control = Java.Lang.Object.GetObject<WebkitWebView>(webView.NativeHandle, JniHandleOwnership.DoNotTransfer);

            if(control == null)
			{
                System.Diagnostics.Debug.WriteLine("No control to print");
				return;            
			}

			try
            {
				using (PrintManager printManager = context.GetSystemService(Context.PrintService) as PrintManager)
				{

					if(Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
					{
					    printManager?.Print(control.Title, control.CreatePrintDocumentAdapter(control.Title), null);               
					}
                    else
					{
						printManager?.Print(control.Title, control.CreatePrintDocumentAdapter(), null);
					}
				}
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
		}
	}

	class FilePrintDocumentAdapter : PrintDocumentAdapter
    {
        private readonly string _fileName;
		private readonly string _filePath;
        private readonly Context context;
        
		public FilePrintDocumentAdapter(Context context, string fileName, string filePath)
        {
			this.context = context;
            _fileName = fileName;
            _filePath = filePath;
        }
      
        public override void OnLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes, CancellationSignal cancellationSignal, LayoutResultCallback callback, Bundle extras)
        {
            if (cancellationSignal.IsCanceled)
            {
                callback.OnLayoutCancelled();
                return;
            }

            callback.OnLayoutFinished(new PrintDocumentInfo.Builder(_fileName)
                .SetContentType(PrintContentType.Document)
                .Build(), true);
        }

        public override void OnWrite(PageRange[] pages, ParcelFileDescriptor destination, CancellationSignal cancellationSignal, WriteResultCallback callback)
        {
            try
            {
                IFileService service = DependencyService.Get<IFileService>();;

                byte[] bytes = service.ReadAllBytes(_filePath);

                using (OutputStream output = new FileOutputStream(destination.FileDescriptor))
                {
                    output.Write(bytes, 0, bytes.Length);
                }

                callback.OnWriteFinished(new[] { PageRange.AllPages });

            }
            catch (Java.IO.FileNotFoundException fileNotFoundException)
            {
                System.Diagnostics.Debug.WriteLine(fileNotFoundException);
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine(exception);
            }
        }
    }
}
