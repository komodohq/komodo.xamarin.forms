﻿using System;
using Android.Content;
using Android.Support.V7.App;
using Komodo.Xamarin.Forms.Services;
using Xamarin.Forms;
using Komodo.Xamarin.Forms.Android.Services;
using Komodo.Xamarin.Forms.ViewModels.Controls;

[assembly: Dependency(typeof(DialogService))]

namespace Komodo.Xamarin.Forms.Android.Services
{
	public class DialogService : IDialogService
    {
		private readonly Context context;

		public DialogService(Context context)
		{
			this.context = context;
		}

		public void Show(DialogViewModel viewModel)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(context);

            AlertDialog dialog = builder.Create();
            dialog.SetTitle(viewModel.Title.Label);
            dialog.SetMessage(viewModel.Message.Label);


            int current = 1;

            foreach (DialogButtonViewModel button in viewModel.Buttons)
            {
                switch (current)
                {
                    case 1:
						dialog.SetButton((int)DialogButtonType.Positive, button.Label, (sender, e) => button.Action());
                        break;
                    case 2:
						dialog.SetButton((int)DialogButtonType.Neutral, button.Label, (sender, e) => button.Action());
                        break;
                    case 3:
						dialog.SetButton((int)DialogButtonType.Negative, button.Label, (sender, e) => button.Action());
                        break;
                }

                current++;
            }

            viewModel.OnClose += (sender, e) => dialog.Cancel();

            dialog.Show();
		}
	}
}
