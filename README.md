# Komodo.Xamarin.Forms

Helper library for including in new Xamarin.Forms projects.

This library provides commonly used ViewModels and Services to be used for composing a new project.

# ViewModels

The ViewModels are generic, localizable and testable virtual representations of their visual counterparts:

* ButtonViewModel.
* CheckBoxViewModel.
* LabelViewModel.
* ListViewModel.
* LocalisableViewModel (abstract).
* RadioButtonViewModel and RadioGroup

The library also provides a basic `Bootstrap` resolver for binding `Views` to `ViewModels`. This can be done by overriding the Bootstrap class and registering your `Views` against `ViewModels` in the constructor (or elsewhere if you want to delay bootstrapping).

```csharp
    public class MyAppBootstrap : Bootstrap
    {
        public MyAppBootstrap() 
        {
            Register<HelpViewModel, HelpView>();
        }
    }

```

When resolving views you need to create the `ViewModel` and call Resolve on the Bootstrap. The Resolve call returns a `Page`. The Resolve call makes sure the Page's BindingContext is set. Basic usage is shown below:

```csharp
IBootstrapService bootstrapService = new MyAppBootstrap();

//create your new page instance
HelpViewModel helpModel = new HelpViewModel("parameters");

//create the page with your model data
Page helpPage = bootstrapSerivce.Resolve(helpModel);

//present the page
navigationPage.PushAsync(helpPage);
```

## Composition

ViewModels are composed of ControlViewModels. These help contain the scope of properties and give a consistant a predictable way of binding to properties. 

```csharp
public class HelpViewModel : LocalizableViewModel 
{
    public LabelViewModel Header { get; set; }
    public LabelViewModel Message { get; set; }
    public ButtonViewModel Close { get; set; }

    public HelpViewModel() 
    {
        Header = new LabelViewModel("pages", "help", "header");
        Message = new LabelViewModel("pages", "help", "message");
        Close = new ButtonViewModel(DoClose, "pages", "help", "close");
    }

    private void DoClose()
    {
        //navigate back
    }
}
```

# Services

The services provided contain a basic set of commonly used features shared across apps. These services can be resolved using the standard `DependancyService` class. The services available are:

* DeviceService. Used for providing interactions with the underlying device.
* DialogService. Provides a means for unifying Dialog constructions between platforms.
* FileService. Provides basic File IO on files provided in the Resources folders.
* LocaleService. Unifies the localization between platforms and provides a means of returning localized strings.
* PrintService. Provides a means of printing documents and WebViews between platforms.
----
## DeviceService

Currently the DeviceService can only open the devices default mail application.

**Usage:**
```csharp
IDeviceService service = DependencyService.Get<IDeviceService>();
service.OpenMailTo("test@test.com", "Title", "This is the email body.");
```
----
## DialogService

**Usage:**
```csharp
DialogViewModel dialog = new DialogViewModel();
dialog.Title = new LabelViewModel("dialogs", "delete_title");
dialog.Message = new LabelViewModel("dialogs", "delete_message");         
dialog.Buttons.Add(new DialogButtonViewModel(() =>
{
    //delete resource
    dialog.Dismiss();

}, "dialogs", "yes"));
          
dialog.Buttons.Add(new DialogButtonViewModel(() =>
{
    //dont delete resource
    dialog.Dismiss();
}, "dialogs", "no"));

dialog.Show();
```
----
## FileService

FileService reads file data from files bundled in the app via the Assets folder (Android) and Resources folder (iOS).

**Usage:**
```csharp
IFileService service = DependencyService.Get<IFileService>();
string text = service.ReadAllText("path/to/file.json");
byte[] data = service.ReadAllBytes("path/to/file");
```

----
## LocaleService

LocaleService provides a unified way of accessing locale info such as LanguageCode, CountryCode and TimeZone. It additionally provides a means of reading localized language json files which are stored in the `[Resources/Assets]/locale` folder via a 2 digit language code. 



```json
Assets/locale/es.json
{
    "my": {
        "localized": {
            "string": "Mi cadena localizada"
        }
    }
}
```

```csharp
IFileService service = DependencyService.Get<IFileService>();
string text = service.ReadAllText("path/to/file.json");
byte[] data = service.ReadAllBytes("path/to/file");

string localizedString = service.GetString("my", "localized", "string");
//localizedString => "Mi cadena localizada"
```

----
## PrintService

The PrintService shows the native Print Preview dialog on supported devices. It can print documents by file path and types of PrintableWebView controls.

```xml
<PrintableWebView x:Name="myPrintableWebView" Source="{Binding HtmlContent, Converter={StaticResource StringToWebViewSource}} />
```
```csharp
IPrintService service = DependencyService.Get<IPrintService>();
service.Show("path/to/file.docx");
//or
service.Show(myPrintableWebView);
```